const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const BigCategory = require('../models/BigCategory')

// @desc    Get big categories
// @route   GET /api/v1/bigCategories
// @access  Public
exports.getBigCategories = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults)
})

// @desc    Get single big category
// @route   GET /api/v1/bigCategories/:id
// @access  Private/Admin
exports.getBigCategory = asyncHandler(async (req, res, next) => {
  const bigCategory = await BigCategory.findById(req.params.id)
    .populate({ path: 'miniCategories' , select: '_id name'});

  if (!bigCategory) {
    return next(
      new ErrorResponse(`No big Category with that id of ${req.params.id}`)
    )
  }

  res.status(200).json({ sucess: true, data: bigCategory })
})

// @desc Add a big category
// @route POST /api/vi/bigCategories
// access Private/Admin
exports.addBigCategory = asyncHandler(async (req, res, next) => {
  let {name} = req.body
  const bigcategory = await BigCategory.create({name})
  res.status(200).json({
    success: true, data: bigcategory
  })
})

// @decs Update a big category
// @route PUT /api/v1/bigCategories/:id
// access Private/Admin
exports.updateBigCategory = asyncHandler(async (req, res, next) => {
  const bigCategory = await BigCategory.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    context: 'query'
  })
  
  if(!bigCategory){
    return next(
      new ErrorResponse(`No big category with that id of ${req.params.id}`)
    )
  }
  
  res.status(200).json({success: true, data: bigCategory})
})

// @decs Delete a big category
// @route DELETE /api/v1/bigCategories/:id
// access Private/Admin
exports.deleteBigCategory = asyncHandler(async (req, res, next) => {
  const bigCategory = await BigCategory.findById(req.params.id)
  

  console.log(bigCategory)

  if (!bigCategory) {
    return next(
      new ErrorResponse(`No big category with that id of ${req.params.id}`)
    )
  }

  await bigCategory.remove()

  res.status(200).json({
    success: true, 
    data: bigCategory
  })
})
