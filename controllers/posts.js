const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const checkPermission = require('../utils/permission')

const Post = require('../models/Post')

// @desc    Get posts
// @route   GET /api/v1/posts/public or /api/v1/posts/private
// @access  Public Or Private
exports.getPosts = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults)
})

// @desc    Get single post
// @route   GET /api/v1/posts/:id
// @access  Public
exports.getPost = asyncHandler(async (req, res, next) => {
  const post = await Post.findById(req.params.id)
    .populate({
      path: 'hashtagId'
    })
    .populate({ path: 'userId', select: 'name imageUrl' })

  if (!post) {
    return next(new ErrorResponse(`No post with that id of ${req.params.id}`))
  }

  res.status(200).json({ sucess: true, data: post })
})

// @decs Add a post
// @route POST /api/v1/posts
// @access Writer
exports.addPost = asyncHandler(async (req, res, next) => {
  let post = {title, body, description, imageUrl, miniCategoryId, videoUrl, hashtagId, status, referenceLink} = req.body
  post.userId = req.user._id
  const result = await Post.create(post)
  res.status(200).json({
    success: true, data: result,
  })
})

//@decs Update a post v1
//@route PUT /api/v1/posts/:id
//access Writer
exports.updatePost = asyncHandler(async(req, res, next) => {
  let post = await Post.findById(req.params.id)

  if (!post) { 
    return next(
      new ErrorResponse(`No post with that id of ${req.params.id}`)
    )
  }

  if (!checkPermission(req.user, post)){
    return next(
      new ErrorResponse(`You don't have permission to make these changes`)
    )
  }
  
  delete req.userId

  await post.updateOne(req.body, {
    new: true,
    runValidators: true,
    context: 'query'
  });

  post = await Post.findById(req.params.id)

  res.status(200).json({
    success: true,
    data: post,
  })
})

// @decs Increase view of a post
// @route PUT /api/v1/posts/incView/:id
//access
exports.updateViews = asyncHandler(async(req, res, next) => {
  const post = await Post.findById(req.params.id)

  if (!post) {
    return next(
      new ErrorResponse(`No post with that id of ${req.params.id}`)
    )
  }

  post.views++
  await post.save()

  res.status(200).json({
    success: true,
    data: post,
  })
})
//@decs Delete a post 
//@route DELETE /api/v1/posts/:id
//access 
exports.deletePost = asyncHandler(async(req, res, next) => {
  const post = await Post.findById(req.params.id)

  if (!post){
    return next(
      new ErrorResponse(`No post with that id of ${req.params.id}`)
    )
  }

  if (!checkPermission(req.user, post)){
    return next(
      new ErrorResponse(`You don't have permission to make these changes`)
    )
  }

  post.status = 'private'

  await post.save()
  
  res.status(200).json({
    success: true,
    data: post,
  })
})
