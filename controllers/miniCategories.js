const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const MiniCategory = require('../models/MiniCategory')

// @desc    Get min categories
// @route   GET /api/v1/miniCategories
// @access  Private/Admin
exports.getMiniCategories = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults)
})

// @desc    Get single mini category
// @route   GET /api/v1/miniCategories/:id
// @access  Private/Admin
exports.getMiniCategory = asyncHandler(async (req, res, next) => {
  const miniCategory = await MiniCategory.findById(req.params.id)

  if (!miniCategory) {
    return next(
      new ErrorResponse(`No mini Category with that id of ${req.params.id}`)
    )
  }

  res.status(200).json({ sucess: true, data: miniCategory })
})
// @desc    Create mini Category
// @route   POST /api/v1/miniCategories/
// @access  Private/Admin
exports.createMiniCategory = asyncHandler(async (req, res, next) => {
  const miniCategory = await MiniCategory.create({
    ...req.body,
    userId: req.user.id
  })

  return res.status(200).json({ sucess: true, data: miniCategory })
})

// @desc    Update mini category
// @route   PUT /api/v1/miniCategories
// @access  Private/Admin
exports.updateMiniCategory = asyncHandler(async (req, res, next) => {
  const miniCategory = await MiniCategory.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    context: 'query'
  })

  if (!miniCategory)
    return next(
      new ErrorResponse(`No mini category with that id of ${req.params.id}`)
    )

  res.status(200).json({ success: true, data: miniCategory })
})

// @desc    Delete mini Category
// @route   DELETE /api/v1/miniCategories/:id
// @access  Private/Admin
exports.deleteMiniCategory = asyncHandler(async (req, res, next) => {
  let miniCategory = await MiniCategory.findById(req.params.id)

  if (!category) {
    return next(
      new ErrorResponse(`No mini category with id of ${req.params.id}`, 404)
    )
  }

  await miniCategory.remove()

  return res.status(200).json({ success: true, data: miniCategory })
})
