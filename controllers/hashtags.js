const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const Hashtag = require('../models/Hashtag')

// @desc    Get hashtags
// @route   GET /api/v1/hashtags
// @access  Private/Admin
exports.getHashtags = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults)
})

// @desc    Get single hashtag
// @route   GET /api/v1/hashtags/:id
// @access  Private/Admin
exports.getHashtag = asyncHandler(async (req, res, next) => {
  const hashtag = await Hashtag.findById(req.params.id)

  if (!hashtag) {
    return next(
      new ErrorResponse(`No hashtag with that id of ${req.params.id}`)
    )
  }

  res.status(200).json({ sucess: true, data: hashtag })
})

// @decs Add a hashtag
// @route POST /api/v1/hashtags/
// @access Writer
exports.addHashtag = asyncHandler(async (req, res, next) => {
  let {name} = req.body
  const hashtag = await Hashtag.create({name})
  res.status(200).json({
    success: true, data: hashtag
  })
})

// @decs Delete a hashtag
// @route DELETE /api/v1/hashtags/:id
// @access Writer
exports.deleteHashtag = asyncHandler(async (req, res, next) => {
  const hashtag = await Hashtag.findByIdAndUpdate(req.params.id, {status: 'private'}, {
    new: true,
    runValidators: true,
    context: 'query'
  })
  
  if(!hashtag){
    return next(
      new ErrorResponse(`No hashtag with that id of ${req.params.id}`)
    )
  }
  
  res.status(200).json({success: true, data: hashtag})
})