const express = require('express')
const {
  getUsers,
  getUser,
  reportUser,
  grantRoleUser,
  blockUser,
  deleteUser,
  blockModerator,
  deleteModerator,
  grantRoleModerator
} = require('../controllers/users')

const User = require('../models/User')

const router = express.Router({ mergeParams: true })

const advancedResults = require('../middleware/advancedResults')
const { protect, authorize } = require('../middleware/auth')



module.exports = router
