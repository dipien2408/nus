const express = require('express')
const {
  getMiniCategories,
  getMiniCategory,
  addMiniCategory,
  updateMiniCategory,
  deleteMiniCategory
} = require('../controllers/miniCategories')

const MiniCategories = require('../models/MiniCategory')

const router = express.Router()

const advancedResults = require('../middleware/advancedResults')
const { protect, authorize } = require('../middleware/auth')



module.exports = router
