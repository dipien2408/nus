const express = require('express')
const {
  getHashtags,
  getHashtag,
  addHashtag,
  deleteHashtag
} = require('../controllers/hashtags')

const Hashtag = require('../models/Hashtag')

const router = express.Router()

const advancedResults = require('../middleware/advancedResults')
const { protect, authorize } = require('../middleware/auth')

router
  .route('/')
  .get(protect, authorize('admin', 'moderator'), advancedResults(Hashtag), getHashtags)
  .post(protect, addHashtag)


router 
  .route('/:id')
  .get(protect, authorize('admin', 'moderator'), getHashtag)
  .delete(protect, authorize('admin', 'moderator'), deleteHashtag)

module.exports = router
