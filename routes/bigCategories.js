const express = require('express')
const {
  getBigCategories,
  getBigCategory,
  addBigCategory,
  updateBigCategory,
  deleteBigCategory
} = require('../controllers/bigCategories')

const BigCategories = require('../models/BigCategory')

const router = express.Router()

const advancedResults = require('../middleware/advancedResults')
const { protect, authorize } = require('../middleware/auth')

router
  .route('/')
  .get(advancedResults(BigCategories), getBigCategories)
  .post(protect, authorize('admin'), addBigCategory)

router
  .route('/:id')
  .put(protect, authorize('admin'), updateBigCategory)
  .delete(protect, authorize('admin'), deleteBigCategory)

module.exports = router
