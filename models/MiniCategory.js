const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const Schema = mongoose.Schema

const MiniCategorySchema = new Schema(
  {
    name: {
      type: String,
      minlength: [3, 'Name must be three characters long'],
      trim: true,
      unique: true,
      uniqueCaseInsensitive: true,
      required: [true, 'Name is required'],
    },
    bigCategoryId: {
      type: Schema.ObjectId,
      ref: 'BigCategory'
    }
  },
  { timestamps: true }
)

MiniCategorySchema.plugin(uniqueValidator, { message: '{PATH} already exists.' })

module.exports = mongoose.model('MiniCategory', MiniCategorySchema)
