const mongoose = require('mongoose')

const Schema = mongoose.Schema

const PostSchema = new Schema(
  {
    title: {
      type: String,
      minlength: [3, 'Must be three characters long']
    },
    body: {
      type: String,
      default: ''
    },
    description: {
      type: String,
      default: ''
    },
    imageUrl: {
      type: String,
      default: 'no-photo.jpg'
    },
    views: {
      type: Number,
      default: 0
    },
    miniCategoryId: {
      type: Schema.ObjectId,
      ref: 'MiniCategory'
    },
    userId: {
      type: Schema.ObjectId,
      ref: 'User',
      required: true
    },
    videoUrl: {
      type: String,
      default: ''
    },
    hashtagId: [
      { 
        type: Schema.ObjectId,
        ref: "Hashtag"
      }
    ],
    status: {
      type: String,
      enum: ['private', 'public'],
      default: 'public'
    },
    referenceLink: [
      {
        type: String,
        default: ''
      }
    ]
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true }, timestamps: true }
)

PostSchema.index({ title: 'text' })

PostSchema.virtual('hashtags', {
  ref: 'Hashtag',
  localField: '_id',
  foreignField: 'postId',
  justOne: false,
  count: true
})

module.exports = mongoose.model('Post', PostSchema)
