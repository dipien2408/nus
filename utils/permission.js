const checkPermission = (user, post) => {
    if (user.role == 'user' && !user._id.equals(post.userId))
        return false
    return true
}

module.exports = checkPermission